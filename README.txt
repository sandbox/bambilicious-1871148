Event Confirm
-------------
Event Confirm is a small module allowing you to let the user confirm triggered
events before taking action. For example, let the user to confirm a "This will
delete _everything_, OK?"-dialog before executing "that sweet AJAX-request".


Installation
------------
Enable the module. However, this is one of those modules that requires more than
configuration to actually take action, please do read API Usage.


API Usage
---------
Event Confirm will look for elements holding the "event-confirm"-class. E.g.:

<a href="http://example.com" class="event-confirm">Conquer!</a>

If you leeve the element at that, a default-dialog will get triggered by the
default-event. To customize these options on a element, you need to extend the
element with data-attributes. E.g.:

<a href="http://example.com" class="event-confirm" data-event-confirm-dialog="
This will coquer the world, OK?">Conquer!</a>

The following options are avaiable as for now:

  data-event-confirm-event:
  The event to bind to.
  
  data-event-confirm-dialog:
  The text populated in the confirm-dialog.

  
Behavior Weights (http://drupal.org/project/behavior_weights)
-------------------------------------------------------------
As the default implementation of Drupal.attachBehaviors can't guarantee
any specific order in which modules behavior-code gets executed, sometimes
Event Confirm's handler-injection can be run to early (resulting in other
actions being triggered before the confirm-dialog is even born).

If you do experience that kind of problem, Behavior Weights might be able to
help you out. Try enabling the module, and possible increase the Weight-value
found on Event Confirm's settings page.