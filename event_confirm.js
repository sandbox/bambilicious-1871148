(function ($) {
  Drupal.behaviors.eventConfirm = {
    attach: function(context, settings) {
      $('.event-confirm:not(.event-confirm-processed)').each(function() {
    	var elem = $(this);
        var events = elem.clone(true).data('events');
        var event = elem.data('event-confirm-event') !== undefined ? elem.data('event-confirm-event') : Drupal.settings.event_confirm.event;
        elem.unbind(event).bind(event, function (e) {
          if (confirm(elem.data('event-confirm-dialog') !== undefined ? elem.data('event-confirm-dialog') : Drupal.settings.event_confirm.dialog)) {
            if (typeof(events) != 'undefined' && typeof(events[event]) != 'undefined') {
              $.each(events[event], function() {
                this.handler(e);
              });
            }
            return true;
          }
          return false;
        });
        elem.addClass('event-confirm-processed');
      });
    },
    weight: Drupal.settings.event_confirm.weight 
  }
})(jQuery);